var through = require('through'),
	chalk = require('chalk'),
	gulpmatch = require('gulp-match'),
	path = require('path'),
	gutil = require('gulp-util');


module.exports = function(options) {
	options = options || {};
	options.sass = !!options.sass;
	options.eol = options.sass ? '' : ';';
	options.ignoreJsonErrors = !!options.ignoreJsonErrors;
	options.breakpoint = '$breakpoint-main';

	return through(processJSON);

	/////////////

	function loadVariablesRecursive(obj, property, callback) {
		for (var key in obj) {

			if (obj.hasOwnProperty(key)) {

				var val = obj[key];

				if (typeof val === 'object') {
					// Recursivity
					// The property become the key
					loadVariablesRecursive(val, key, callback);
				}
				else if (key.match(/[@]/g)) {
					key = key.replace('@', '\\@');
					callback('.u-' + key + '{ @media(min-width:' + options.breakpoint + '){' + property + ':' + val + options.eol + '}}');
				}
				else {
					callback('.u-' + key + '{ ' + property + ':' + val + options.eol + ' }');
				}
			}
		}
	}

	function processJSON(file) {

		// Check the file (if it does not have a .json suffix, ignore it)
		// -------
		if (!gulpmatch(file,'**/*.json')) {
			this.push(file);
			return;
		}

		// Load the JSON
		// -------
		try {
			var parsedJSON = JSON.parse(file.contents);
		} catch (e) {
			if (options.ignoreJsonErrors) {
				console.log(chalk.red('[gulp-json-sass]') + ' Invalid JSON in ' + file.path + '. (Continuing.)');
			} else {
				console.log(chalk.red('[gulp-json-sass]') + ' Invalid JSON in ' + file.path);
				this.emit('error', e);
			}
			return;
		}

		// Process the JSON
		// -------

		// the callback function is to put the utility in an array
		var sassUtilitiesArray = [];

		loadVariablesRecursive(parsedJSON, '', function pushVariable(assignmentString) {
			sassUtilitiesArray.push(assignmentString);
		});

		// then join the array
		var sass = sassUtilitiesArray.join('\n');

		// export the fil
		file.contents = Buffer(sass);
		file.path = gutil.replaceExtension(file.path, options.sass ? '.sass' : '.scss');

		this.push(file);
	}

}
