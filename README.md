# gulp-yml-to-sass-utilities

Converting formated Json 'tokens' to Sass utility classes.

## Usage

`gulpfile.js`

```
'use strict';

var gulp = require('gulp'),
    jsonSassUtilities = require('gulp-json-to-sass-utilities');

gulp.task('default', function() {
    return gulp.src('tokens.json')
        .pipe(jsonSassUtilities())
        .pipe(gulp.dest('output'));
});
```

See the test for an example using yaml tokens.

## Running test

Install gulp

`npm install --global gulp-cli`

Install plugin dependencies

`npm install`

Install test dependencies

```
cd test
npm install
```

Start test

```
gulp
```

## Cases

### Supported

- Basic utility with or without variable

```
.u-bold {
    font-weight: $font-weight-bold;
}

.u-pd-skew {
    padding-bottom: (tan(8deg) * 100vw);
}
```

- `!important` specificity

```
.u-normal {
    font-weight: normal !important;
}
```

- Utilities starting at the main breakpoint

```
.u-block@main {
    @media (min-width: $breakpoint-main) {
        display: block;
    }
}
```

### Non supported (yet)

- `@include` utilities

```
.u-reset-ul {
    @include reset-ul();
}
```

- Multiple properties utilities:

```
.u-object-fit-cover {
    object-fit: cover;
    font-family: "object-fit: cover";
}
```

- Selector with depth:

```
.u-svg-white g {
    fill: $white;
}
```

