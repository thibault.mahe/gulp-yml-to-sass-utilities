'use strict';

var footer = require('gulp-footer'),
	gulp = require('gulp'),
	header = require('gulp-header'),
	jsonSassUtilities = require('../index'),
    yaml = require('gulp-yaml');

/**
 * Paths
 */
var paths = {
	tokens: {
		src: '_utilities.yml',
		outputFolder: '_generated',
		jsonOutputFolder: '_generated',
	},

};

/**
 * Compile Design tokens
 */

gulp.task('build-utilities', function() {
	return gulp.src(paths.tokens.src)
		// transform tokens from yaml into json
		.pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
		// store json
		.pipe(gulp.dest(paths.tokens.jsonOutputFolder))
		// transform tokens from json into scss
		.pipe(jsonSassUtilities())
		// Add header and footer to the files
		.pipe(footer('\n'))
		.pipe(header('// Design utilities generated from ./_design-tokens\n'))
		// store to output folder
		.pipe(gulp.dest(paths.tokens.outputFolder));
});

gulp.task('default', ['build-utilities']);
